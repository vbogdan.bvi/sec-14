/**
 * Created by vbogdan on 20/09/2021.
 */

trigger sec14_5_trigger on Resource__c (after insert, after update) {
    sec14_5_trHandler.checkContact(Trigger.new);

}