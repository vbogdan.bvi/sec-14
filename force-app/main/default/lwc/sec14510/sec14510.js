/**
 * Created by vbogdan on 20/09/2021.
 */

import {LightningElement, api, wire, track} from 'lwc';
import getResourcesByConId from '@salesforce/apex/sec14510LwcController.getResourcesByConId';


const columns = [
    { label: 'Id', fieldName: 'Id' },
    { label: 'Name', fieldName: 'Name' },
    { label: 'Estimate Cost', fieldName: 'Estimate_Cost__c' },
    { label: 'Model__c', fieldName: 'Model__c' },
    { label: 'Type__c', fieldName: 'Type__c' },
];

export default class Sec14510 extends LightningElement {
    @track resources;
    @track error;
    @api recordId;

    columns = columns;


    connectedCallback() {
        console.log('connectedCallback');
            getResourcesByConId({ conId: this.recordId })
                .then(result => {
                    console.log('---getRes---then')
                    this.resources = result;
                    this.error = null;

                })
                .catch(error => {
                    console.log('---getRes---catch')
                    this.error = error;
                    this.resources = null;
                })
    }


}
