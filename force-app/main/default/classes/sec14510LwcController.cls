/**
 * Created by vbogdan on 20/09/2021.
 */

public with sharing class sec14510LwcController {

    @AuraEnabled
    public static List<Resource__c> getResourcesByConId(Id conId) {
        System.debug('getResourcesByConId');
        return [SELECT id, Name, Estimate_Cost__c, Type__c, Model__c FROM Resource__c WHERE Contact__c = :conId];
    }

}
