/**
 * Created by vbogdan on 20/09/2021.
 */

public with sharing class sec14_5_trHandler {
    public static void checkContact(List<Resource__c> resources) {
        List<Id> conIds = new List<Id>();
        for (Resource__c res : resources) {
            if (!conIds.contains(res.Contact__c)) {
                conIds.add(res.Contact__c);
            }
        }

        List<Contact> contacts = [
                SELECT Id, Responsibility__c, (
                        SELECT id, Estimate_Cost__c
                        FROM Resources__r
                )
                FROM Contact
                WHERE id in :conIds
        ];

        Material_Settings__c mc = Material_Settings__c.getOrgDefaults();

        for (Contact con : contacts) {
            if (con.Resources__r.size() > 0) {
                Decimal sumEst = 0 ;
                for (Resource__c res : con.Resources__r) {
                    sumEst += res.Estimate_Cost__c;
                }

                if (sumEst >= mc.High_Responsibility_Cost__c) {
                    con.Responsibility__c = 'High';
                } else {
                    con.Responsibility__c = 'Low';

                }
            } else {
                con.Responsibility__c = 'Low';
            }
        }

        update contacts;
    }
}