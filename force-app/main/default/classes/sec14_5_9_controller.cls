/**
 * Created by vbogdan on 20/09/2021.
 */

public with sharing class sec14_5_9_controller {
    public List<Resource__c> resources {get; set;}
    public List<SelectOption> selectOptions {get; set;}
    public String typeRes {get; set;}

    public sec14_5_9_controller() {
        resources = [SELECT id, Name, Contact__r.Name, Type__c, Estimate_Cost__c, Model__c, Inventory_Number__c from Resource__c];

       selectOptions = new List<SelectOption>();
        selectOptions.add(new SelectOption('Equipment','Equipment'));
        selectOptions.add(new SelectOption('Furniture','Furniture'));
        selectOptions.add(new SelectOption('Car','Car'));
    }

    public void loadRes() {
        System.debug('Type - ' + typeRes);
        resources = [SELECT id, Name, Contact__r.Name, Type__c, Estimate_Cost__c, Model__c, Inventory_Number__c from Resource__c WHERE Type__c=:typeRes];
    }


}